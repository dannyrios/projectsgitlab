package poo;

public class Persona extends Animal{
	
	private String cedula;

	public Persona() {
		
	}

	public Persona(String nombre, String estatura, String edad, String cedula) {
		super(nombre, estatura, edad);
		this.cedula = cedula;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public void pensar() {
		System.out.println("La persona realiza la acci�n pensar");
	}

}
