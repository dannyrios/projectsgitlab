package poo;

public class Animal {
	
	private String nombre;
	private String estatura;
	private String edad;
	
	public Animal(String nombre, String estatura, String edad) {
		this.nombre = nombre;
		this.estatura = estatura;
		this.edad = edad;
		
	}
	
	public Animal() {
		
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstatura() {
		return estatura;
	}

	public void setEstatura(String estatura) {
		this.estatura = estatura;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public void comer()
	{
		System.out.println("Las personas y las mascotas comen");
	}
	
	public void dormir()
	{
		System.out.println("Las personas y las mascotas duermen");
	}
	
	
}
