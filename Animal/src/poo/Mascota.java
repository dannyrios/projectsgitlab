package poo;

public class Mascota extends Animal{
	
	private String especie;

	public Mascota() {
		
	}
	
	public Mascota(String nombre, String estatura, String edad, String especie) {
		super(nombre, estatura, edad);
		this.especie = especie;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}
	
	public void pasear() {
		System.out.println("La mascota realiza la acci�n pasear");
	}
	
	

}
