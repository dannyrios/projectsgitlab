package poo;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static ArrayList<Animal> seres = new ArrayList<Animal>();
	private static Scanner entrada;
	
	public static void main(String[] args) {
		System.out.println("Seleccione: \n1.Persona \n2.Animal");
		entrada = new Scanner(System.in);
        int opc=entrada.nextInt(); 
        seleccionar(opc);
        
		//COMER
		for(Animal ser : seres) {
			ser.comer();
			System.out.println(ser.getNombre() + " " + ser.getEdad() + " " + ser.getEstatura() + " -> ");
		}
		
		//DORMIR
		for(Animal ser : seres) {
			ser.dormir();
			System.out.println(ser.getNombre() + " " + ser.getEdad() + " " + ser.getEstatura() + " -> ");
		}
	}
	
	public static void seleccionar(int opc)
	{
		if(opc==1)
        {
        	Persona persona = new Persona("Juan", "1.60", "23", "23323453");
        	seres.add(persona);
        	//PENSAR
        	persona.pensar();
    		System.out.println(persona.getNombre()+" "+persona.getEstatura()+" "+persona.getEdad()+" "+persona.getCedula());
        }
        else if(opc==2) {
        	Mascota mascota = new Mascota("Pancho", "1.03", "3", "gato");
        	seres.add(mascota);
        	//PASEAR
        	mascota.pasear();
    		System.out.println(mascota.getNombre()+" "+mascota.getEstatura()+" "+mascota.getEdad()+" "+mascota.getEspecie());
        }
        else {
        	System.out.println("Opcion Incorrecta!");
        }
	}

}
