package com.accenture.TwitterCucumberGradle.step_definitions;

import com.accenture.TwitterCucumberGradle.steps.StepTwitter;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepsDefinition {
	@Steps
	StepTwitter Twit;
	
	@Given("^que developer acc abrio el navegador en la pagina de twitter$")
    public void que_developer_acc_abrio_el_navegador_en_la_pagina_de_twitter() throws Throwable {
       
		Twit.press_login();
    }
	
	 @And("^ingresa las credenciales de (.*) con (.*)")
	 public void ingresa_las_credenciales(String username, String pass) throws Throwable {
	    Twit.enter_credentials(username,pass);
	 }

    @When("^developer acc quiere buscar a (.*) y enviarle un twitter que diga (.*)$")
    public void developer_acc_quiere_seguir_y_twittear_a_daniel_correa(String busqueda, String commentary) throws Throwable {
    	Twit.enter_keywords(busqueda);
    	Twit.select_result();
    	Twit.follow();
    	Twit.tweet();
        Twit.post(commentary);
        Twit.unfolow();
        
    }

    @Then("^(.*) espera haber enviado su mensaje a Daniel Correa diciendo (.*)$")
    public void developer_acc_espera_haber_enviado_su_mensaje_a_daniel_correa(String search, String comparar) throws Exception {
        Twit.enter_profile(search);
        Twit.select_profile();
        Twit.show_replies();
        Twit.capture(comparar);
        Twit.select_options();
        Twit.delete_tweet();
        Twit.confirm_delete();
    }

}
 