package com.accenture.TwitterCucumberGradle.steps;

import com.accenture.TwitterCucumberGradle.pages.LoginPage;
import com.accenture.TwitterCucumberGradle.pages.StartPage;
import com.accenture.TwitterCucumberGradle.pages.TwittPage;
import com.accenture.TwitterCucumberGradle.pages.UndoPage;
import com.accenture.TwitterCucumberGradle.pages.ValidatePage;

import net.thucydides.core.annotations.Step;

public class StepTwitter {
	LoginPage log;
	StartPage search;
	TwittPage twitt;
	ValidatePage val;
	UndoPage undo;
	
	@Step
	public void press_login() {
		log.open();
		log.press_login();
	}
	
	@Step
	public void enter_credentials(String username, String pass) {
		log.enter_credentials(username, pass);
		
	}
	
	@Step
	public void enter_keywords(String busqueda) {
		search.enter_keywords(busqueda);
		
	}
	
	@Step
	public void select_result() {
		search.select_result();
		
	}
	
	@Step
	public void follow() {
		twitt.follow();
	}
	
	@Step
	public void tweet() {
		twitt.tweet();
	}
	
	@Step
	public void post(String commentary) {
		twitt.post(commentary);
	}
	
	@Step
	public void unfolow() {
		twitt.unfollow();
	}
	
	@Step
	public void enter_profile(String search){
		val.enter_profile(search);
	}
	
	@Step
	public void select_profile() {
		val.select_profile();
	}
	
	@Step
	public void show_replies() {
		val.show_replies();
	}
	
	@Step
	public void capture(String comparar) {
		val.capture(comparar);
	}
	
	@Step
	public void select_options() {
		undo.select_options();
	}
	
	@Step
	public void delete_tweet() {
		undo.delete_tweet();
	}
	
	@Step
	public void confirm_delete() {
		undo.confirm_delete();
	}
	
}
