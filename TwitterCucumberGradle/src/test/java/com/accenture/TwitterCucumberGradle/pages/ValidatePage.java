package com.accenture.TwitterCucumberGradle.pages;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class ValidatePage extends PageObject {
	
	boolean validar;
	
	@FindBy(xpath = "//input[@class='search-input']") 
	private WebElementFacade buscar;
	
	@FindBy(xpath="//span[contains(text(),'Developer Acc')]")
	private WebElementFacade select;
	
	@FindBy(xpath="//a[@class='ProfileHeading-toggleLink js-nav']")
	private WebElementFacade replies;
	
	public void enter_profile(String search) {
	    buscar.sendKeys(search);
	 }
	
	public void select_profile() {
		select.withTimeoutOf(5, TimeUnit.SECONDS);
		select.waitUntilVisible().click();
	}
	
	public void show_replies() {
		replies.withTimeoutOf(5, TimeUnit.SECONDS);
		replies.waitUntilVisible().click();
	}
	
	public void capture(String comparar){
		WebElement texto = getDriver().findElement(By.xpath("//p[@class='TweetTextSize TweetTextSize--normal js-tweet-text tweet-text']"));
		 
		 validar = texto.getText().equals(comparar);
		 
		 if(validar) {
			assertTrue(true);
			System.out.println("La publicacion del Tweet fue exitosa =)");
			System.out.println("El mensaje es " + texto.getText());
		}
		else {
			assertTrue(false);
			System.out.println("Error! No se pudo publicar el tweet =(");
		}
	}

}
