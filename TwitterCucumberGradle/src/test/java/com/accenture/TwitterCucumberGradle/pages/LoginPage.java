package com.accenture.TwitterCucumberGradle.pages;

import java.util.concurrent.TimeUnit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://twitter.com/")
public class LoginPage extends PageObject {
	
	@FindBy(xpath = "//a[@class='js-nav EdgeButton EdgeButton--medium EdgeButton--secondary StaticLoggedOutHomePage-buttonLogin']")
	  private WebElementFacade login;
	
	@FindBy(xpath="//input[@class='js-username-field email-input js-initial-focus']")
	private WebElementFacade user;
	@FindBy(xpath="//input[@class='js-password-field']")
	private WebElementFacade pass;
		
	public void press_login(){
		login.withTimeoutOf(5, TimeUnit.SECONDS);
		login.waitUntilVisible().click();
		try {
			Thread.sleep(4000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	 }
	
	public void enter_credentials(String username, String password) {
		user.sendKeys(username);
	    this.pass.waitUntilClickable().typeAndEnter(password);
	}
	
}

