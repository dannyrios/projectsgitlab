package com.accenture.TwitterCucumberGradle.pages;

import java.util.concurrent.TimeUnit;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class TwittPage extends PageObject {
	
	@FindBy(xpath ="//div[contains(@class,'ProfileCanopy ProfileCanopy--withNav js-variableHeightTopBar')]//span[contains(@class,'user-actions-follow-button js-follow-btn follow-button')]//button[1]")		
	private WebElementFacade seguir;
	@FindBy(xpath ="//span[contains(@class,'NewTweetButton-text')]")		
	private WebElementFacade twitt;
	@FindBy(xpath ="//div[@id='tweet-box-global']")
	private WebElementFacade text;
	@FindBy(xpath ="//form[@class='t1-form tweet-form']//span[@class='button-text tweeting-text'][contains(text(),'Tweet')]")
	private WebElementFacade pub;
	@FindBy(xpath ="//span[@class='user-actions-follow-button js-follow-btn follow-button']")
	private WebElementFacade nseguir;
	
	public void follow()
	{
		seguir.withTimeoutOf(5, TimeUnit.SECONDS);
		seguir.waitUntilVisible().click();
	}
	
	public void tweet()
	{
		twitt.withTimeoutOf(5, TimeUnit.SECONDS);
		twitt.waitUntilVisible().click();
	}
	
	public void post(String commentary)
	{
		text.sendKeys(commentary);
		pub.withTimeoutOf(5, TimeUnit.SECONDS);
		pub.waitUntilVisible().click();
	}
		
	public void unfollow()
	{
		nseguir.withTimeoutOf(5, TimeUnit.SECONDS);
		nseguir.waitUntilVisible().click();
	}
	
}
