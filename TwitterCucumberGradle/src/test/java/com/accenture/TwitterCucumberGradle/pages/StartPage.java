package com.accenture.TwitterCucumberGradle.pages;

import java.util.concurrent.TimeUnit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class StartPage extends PageObject{
	
	@FindBy(xpath = "//input[@class='search-input']") 
	private WebElementFacade buscar;
	
	@FindBy(xpath="//span[@class='fullname'][contains(text(),'Daniel Correa')]")
	private WebElementFacade select;
	
	public void enter_keywords(String busqueda) {
	    buscar.sendKeys(busqueda);
	 }
	
	public void select_result() {
		select.withTimeoutOf(5, TimeUnit.SECONDS);
		select.waitUntilVisible().click();
	}

}
