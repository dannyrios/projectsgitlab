package com.accenture.TwitterCucumberGradle.pages;

import java.util.concurrent.TimeUnit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class UndoPage extends PageObject {
	
	@FindBy(xpath="//span[@class='Icon Icon--caretDownLight Icon--small']")
	private WebElementFacade options;
	@FindBy(xpath="//button[contains(text(),'Delete Tweet')]")
	private WebElementFacade borrar;
	@FindBy(xpath="//button[@class='EdgeButton EdgeButton--danger delete-action']")
	private WebElementFacade delete;
	
	public void select_options(){
		options.withTimeoutOf(5, TimeUnit.SECONDS);
		options.waitUntilVisible().click();
	}
	
	public void delete_tweet() {
		borrar.withTimeoutOf(5, TimeUnit.SECONDS);
		borrar.waitUntilVisible().click();
	}
	
	public void confirm_delete() {
		delete.withTimeoutOf(5, TimeUnit.SECONDS);
		delete.waitUntilVisible().click();
	}
	

}
