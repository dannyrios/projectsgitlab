package pqt;
import javax.swing.*;

public class Inicio {
	
	public static void main(String[] args)
	{
		
		// obtener datos de tutor
		String codTut = JOptionPane.showInputDialog(null, "Codigo del tutor");
		String nomTut = JOptionPane.showInputDialog(null, "Nombre del tutor");
		String cursoTut = JOptionPane.showInputDialog(null, "Curso del tutor");
		Tutor tutor = new Tutor(codTut, nomTut, cursoTut); 
		
		// obtener datos de aprendiz
		String codAp = JOptionPane.showInputDialog(null, "Codigo del aprendiz");
		String nomAp = JOptionPane.showInputDialog(null, "Nombre del aprendiz");
		String cursoAp = JOptionPane.showInputDialog(null, "Curso del aprendiz");
		float notaAp = Float.parseFloat(JOptionPane.showInputDialog(null, "Nota del aprendiz"));
		Aprendiz aprendiz = new Aprendiz(codAp, nomAp, cursoAp, notaAp);
		
		//operaciones con tutor y aprendiz
		if(aprendiz.ganoCurso())
		{
			JOptionPane.showMessageDialog(null, aprendiz.getNombre() + " ha ganado la materia de " + aprendiz.getCurso());
		}
		else
		{
			JOptionPane.showMessageDialog(null, aprendiz.getNombre() + " ha perdido la materia de " + aprendiz.getCurso(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		float nuevaNota = Float.parseFloat(JOptionPane.showInputDialog(null, "Nota a modificar"));
		tutor.modificarNota(aprendiz, nuevaNota);
		JOptionPane.showMessageDialog(null, aprendiz.getNombre() + " tiene una nota de " + aprendiz.getNota());
		//System.out.println(aprendiz.getNombre() + " tiene una nota de " + aprendiz.getNota());
		if(!aprendiz.ganoCurso())
		{
			JOptionPane.showMessageDialog(null, "El tutor" + tutor.getNombre() + " puso a perder al aprendiz " + aprendiz.getNombre() + " el curso de " + aprendiz.getCurso() + " con una nota de " + aprendiz.getNota(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
