package pqt;

public class Tutor {
	
	private String codigo;
	private String nombre;
	private String cursos;
	
	
	
	public Tutor(String cod, String nombre, String cursos) {
		this.codigo = cod;
		this.nombre = nombre;
		this.cursos = cursos;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCursos() {
		return cursos;
	}

	public void setCursos(String cursos) {
		this.cursos = cursos;
	}
	
	public void modificarNota(Aprendiz aprendiz, float nuevaNota)
	{
		aprendiz.setNota(nuevaNota);
	}
	

}
