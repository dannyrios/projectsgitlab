package pqt;

public class Aprendiz {
	
	private String codigo;
	private String nombre;
	private String curso;
	private float nota;
	
	public Aprendiz(String codigo, String nombre, String curso, float nota) {

		this.codigo = codigo;
		this.nombre = nombre;
		this.curso = curso;
		this.nota = nota;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCurso() {
		return curso;
	}

	private void setCurso(String curso) {
		this.curso = curso;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}
	
	public void matricular(String curso)
	{
		this.setCurso(curso);
	}
	
	public boolean ganoCurso() {
		if(this.nota >= 3) {
			return true;
		}
		return false;
	}
	
	
	

}
