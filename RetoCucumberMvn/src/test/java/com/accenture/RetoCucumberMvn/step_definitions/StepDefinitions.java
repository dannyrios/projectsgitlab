package com.accenture.RetoCucumberMvn.step_definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import com.accenture.RetoCucumberMvn.steps.StepFacebook;
import com.accenture.RetoCucumberMvn.steps.StepYoutube;
import net.thucydides.core.annotations.Steps;

public class StepDefinitions {
	
	@Steps
	StepYoutube ytb;
	StepFacebook fb;
	
		@Given("^usuario abrio el navegador en la pagina de Youtube$")
	    public void usuario_abrio_el_navegador_en_la_pagina_de_youtube() throws Throwable {
	        ytb.is_the_youtube_page();
	    }
		
		@And("^busca el video (.*)$")
	    public void busca_el_video_disfruto_electronica(String busqueda) throws Throwable {
	        ytb.is_enter_search(busqueda);
	        ytb.open_video();
	    }
	
	    @When("^usuario quiere compartir el video en Facebook ingresando el correo (.*) con (.*)$")
	    public void usuario_quiere_compartir_el_video_en_facebook(String mail, String password) throws Throwable {
	    	 ytb.share_video(mail, password);
	    	//fb.is_the_facebook_page(mail, password);
	    }
	
	    @Then("^usuario espera haber compartido el video (.*) en Facebook$")
	    public void usuario_espera_haber_compartido_el_video_en_facebook() throws Exception {
	        
	    }
}
