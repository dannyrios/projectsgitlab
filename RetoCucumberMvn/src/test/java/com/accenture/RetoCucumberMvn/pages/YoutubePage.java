package com.accenture.RetoCucumberMvn.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.youtube.com/")
public class YoutubePage extends PageObject{
	@FindBy(xpath = "//input[@id='search']") private WebElementFacade buscar;
	
	public void enter_keywords(String busqueda) {
	    buscar.waitUntilClickable().typeAndEnter(busqueda);
	    
	 }
}
