package com.accenture.RetoCucumberMvn.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class SharePage extends PageObject{
	
	@FindBy(xpath="//div[@id='top-level-buttons']//ytd-button-renderer[1]//a[1]")
	private WebElementFacade compartir;
	@FindBy(xpath="//div[@id='contents']//ytd-share-target-renderer[2]//button[1]//yt-icon[1]")
	private WebElementFacade abrirFB;
	
	@FindBy(xpath="//input[@id='email']")
	private WebElementFacade user;
	@FindBy(xpath ="//input[@id='pass']")
	private WebElementFacade pass;
	
	@FindBy(xpath="//span[@id='u_0_1w']")
	private WebElementFacade publicar;
	
	@FindBy(xpath="//a[@class='_52c6']")
	private WebElementFacade comparar;
	
	public void share_video(String mail, String password) {
		compartir.withTimeoutOf(5, TimeUnit.SECONDS);
		compartir.waitUntilVisible().click();
		abrirFB.withTimeoutOf(5, TimeUnit.SECONDS);
		abrirFB.waitUntilVisible().click();
		
		List<String> allWindowHandles = new ArrayList<String>(getDriver().getWindowHandles());
		String fbWindow = allWindowHandles.get(1);
		getDriver().switchTo().window(fbWindow);
		
		//Login de Facebook
		user.sendKeys(mail);
	    this.pass.waitUntilClickable().typeAndEnter(password);
	    
	    //Publicación del video en el muro
	    publicar.withTimeoutOf(5, TimeUnit.SECONDS);
		publicar.waitUntilVisible().click();
		
		//Abrir nuevamente facebook
		String url = "https://www.facebook.com/profile.php?id=100030385241365";
		getDriver().get(url);
		
		/*Comparar publicación
		String href = comparar.getAttribute("href");
		System.out.print(href);
		assertEquals(href, "https://www.youtube.com/?reload=9&gl=CO&hl=es-419");*/
	}
}
