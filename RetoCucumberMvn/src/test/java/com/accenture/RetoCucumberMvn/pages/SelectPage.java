package com.accenture.RetoCucumberMvn.pages;

import java.util.concurrent.TimeUnit;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class SelectPage extends PageObject{
	@FindBy(xpath="//div[@id='contents']//ytd-video-renderer[1]//div[1]//ytd-thumbnail[1]//a[1]//yt-img-shadow[1]//img[1]") 
	               //div[@id='contents']//ytd-video-renderer[3]//div[1]//ytd-thumbnail[1]//a[1]//yt-img-shadow[1]//img[1]               
	private WebElementFacade abrir;
	
	public void open_video() {
		abrir.withTimeoutOf(5, TimeUnit.SECONDS);
		abrir.waitUntilVisible().click();
	}
	
	//no me gusta este comentario
}
