package com.accenture.RetoCucumberMvn.steps;

import com.accenture.RetoCucumberMvn.pages.SelectPage;
import com.accenture.RetoCucumberMvn.pages.SharePage;
import com.accenture.RetoCucumberMvn.pages.YoutubePage;
import net.thucydides.core.annotations.Step;

public class StepYoutube {
	YoutubePage youtube;
	SelectPage select;
	SharePage shr;
	
	@Step
	public void is_the_youtube_page() {
		youtube.open();
	}
	
	@Step
	public void is_enter_search(String busqueda) {
		youtube.enter_keywords(busqueda);
	}
	
	@Step
	public void open_video() {
		select.open_video();
	}
	
	@Step
	public void share_video(String mail, String password) {
		shr.share_video(mail, password);
	}
	
	/*@Step
	public void open_facebook(String mail, String password) {
		shr.open_facebook(mail, password);
	}*/
}
